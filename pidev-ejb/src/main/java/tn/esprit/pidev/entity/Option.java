package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
@Entity
public class Option extends Contract implements Serializable{
	private Date last_trade_date;
	private float strike;
	private float last_price;
	private float bid;
	private float ask;
	private float degreeChange;
	private int volume;
	private int open_interest;
	private float implied_volatility;

	public Date getLast_trade_date() {
		return last_trade_date;
	}

	public void setLast_trade_date(Date last_trade_date) {
		this.last_trade_date = last_trade_date;
	}

	public float getStrike() {
		return strike;
	}

	public void setStrike(float strike) {
		this.strike = strike;
	}
	
	public float getLast_price() {
		return last_price;
	}

	public void setLast_price(float last_price) {
		this.last_price = last_price;
	}
	
	public float getBid() {
		return bid;
	}

	public void setBid(float bid) {
		this.bid = bid;
	}
	
	public float getAsk() {
		return ask;
	}

	public void setAsk(float ask) {
		this.ask = ask;
	}
	
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	public int getOpen_interest() {
		return open_interest;
	}

	public void setOpen_interest(int open_interest) {
		this.open_interest = open_interest;
	}

	public float getImplied_volatility() {
		return implied_volatility;
	}

	public void setImplied_volatility(float implied_volatility) {
		this.implied_volatility = implied_volatility;
	}

	public float getDegreeChange() {
		return degreeChange;
	}

	public void setDegreeChange(float degreeChange) {
		this.degreeChange = degreeChange;
	}

	
	/*public float getChange() {
		return change;
	}

	public void setChange(float change) {
		this.change = change;
	}
	*/
	/*

	
	

	

	
	
	
*/
}
