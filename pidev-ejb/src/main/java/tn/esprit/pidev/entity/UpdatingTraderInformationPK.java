package tn.esprit.pidev.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
@Embeddable
public class UpdatingTraderInformationPK implements Serializable{
	private int idAdmin;
	private int idTrader;
	public int getIdAdmin() {
		return idAdmin;
	}
	public void setIdAdmin(int idAdmin) {
		this.idAdmin = idAdmin;
	}
	public int getIdTrader() {
		return idTrader;
	}
	public void setIdTrader(int idTrader) {
		this.idTrader = idTrader;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idAdmin;
		result = prime * result + idTrader;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdatingTraderInformationPK other = (UpdatingTraderInformationPK) obj;
		if (idAdmin != other.idAdmin)
			return false;
		if (idTrader != other.idTrader)
			return false;
		return true;
	}
	

}
