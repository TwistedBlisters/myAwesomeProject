package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Share implements Serializable {
	@Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	private int id_share;
	private Date date;
	private float open;
	private float high;
	private float close;
	private String adj_close;
	private float volume;
	@ManyToOne
	private Company company;
	

	
	 public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public float getOpen() {
		return open;
	}
	public float getHigh() {
		return high;
	}
	public float getClose() {
		return close;
	}
	public int getId_share() {
		return id_share;
	}
	public void setId_share(int id_share) {
		this.id_share = id_share;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public float isOpen() {
		return open;
	}
	public void setOpen(float open) {
		this.open = open;
	}
	public float isHigh() {
		return high;
	}
	public void setHigh(float high) {
		this.high = high;
	}
	public float isClose() {
		return close;
	}
	public void setClose(float close) {
		this.close = close;
	}
	public String getAdj_close() {
		return adj_close;
	}
	public void setAdj_close(String adj_close) {
		this.adj_close = adj_close;
	}
	public float getVolume() {
		return volume;
	}
	public void setVolume(float volume) {
		this.volume = volume;
	}

}
