package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Contract implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
private int id_contract;

private float value_of_contract;
private int number_of_shares;
private String status;
private Date contract_date;
@ManyToOne
private Admin admin;
@ManyToOne
private Trader trader;
@ManyToOne
private Company company ;



public Company getCompany() {
	return company;
}
public void setCompany(Company company) {
	this.company = company;
}
public Admin getAdmin() {
	return admin;
}
public void setAdmin(Admin admin) {
	this.admin = admin;
}
public Trader getTrader() {
	return trader;
}
public void setTrader(Trader trader) {
	this.trader = trader;
}
public int getId_contract() {
	return id_contract;
}
public void setId_contract(int id_contract) {
	this.id_contract = id_contract;
}
public float getValue_of_contract() {
	return value_of_contract;
}
public void setValue_of_contract(float value_of_contract) {
	this.value_of_contract = value_of_contract;
}
public int getNumber_of_shares() {
	return number_of_shares;
}
public void setNumber_of_shares(int number_of_shares) {
	this.number_of_shares = number_of_shares;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Date getContract_date() {
	return contract_date;
}
public void setContract_date(Date contract_date) {
	this.contract_date = contract_date;
}

}
