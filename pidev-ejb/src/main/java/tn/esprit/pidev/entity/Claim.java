package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Claim implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_claim;
	private String Subject_Claim;
	private String status;
	private Date date_claim;
	private String description_claim;
	@ManyToOne
	private User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getId_claim() {
		return id_claim;
	}
	public void setId_claim(int id_claim) {
		this.id_claim = id_claim;
	}
	public String getSubject_Claim() {
		return Subject_Claim;
	}
	public void setSubject_Claim(String subject_Claim) {
		Subject_Claim = subject_Claim;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDate_claim() {
		return date_claim;
	}
	public void setDate_claim(Date date_claim) {
		this.date_claim = date_claim;
	}
	public String getDescription_claim() {
		return description_claim;
	}
	public void setDescription_claim(String description_claim) {
		this.description_claim = description_claim;
	}
	

}
