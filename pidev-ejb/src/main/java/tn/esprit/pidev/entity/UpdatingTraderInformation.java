package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class UpdatingTraderInformation implements Serializable {
	@EmbeddedId

	private UpdatingTraderInformationPK information_PK;
	private Date date_of_modification;
	
	@ManyToOne
	@JoinColumn(name="idAdmin",referencedColumnName="id",insertable=false,updatable=false)
	private Admin admin;
	
	@ManyToOne
	@JoinColumn(name="idTrader",referencedColumnName="id",insertable=false,updatable=false)
	private Trader trader;
	
	public Date getDate_of_modification() {
		return date_of_modification;
	}
	public void setDate_of_modification(Date date_of_modification) {
		this.date_of_modification = date_of_modification;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public Trader getTrader() {
		return trader;
	}
	public void setTrader(Trader trader) {
		this.trader = trader;
	}
	public UpdatingTraderInformationPK getInformation_PK() {
		return information_PK;
	}
	public void setInformation_PK(UpdatingTraderInformationPK information_PK) {
		this.information_PK = information_PK;
	}
	

}
