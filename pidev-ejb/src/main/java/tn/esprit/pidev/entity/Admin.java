package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
@Entity
public class Admin extends User implements Serializable{
	private String degree;
	@OneToMany(mappedBy="admin")
	private List<UpdatingTraderInformation>informations;

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public List<UpdatingTraderInformation> getInformations() {
		return informations;
	}

	public void setInformations(List<UpdatingTraderInformation> informations) {
		this.informations = informations;
	}




}
