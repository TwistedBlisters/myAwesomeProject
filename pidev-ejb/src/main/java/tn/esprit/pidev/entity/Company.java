package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Company  implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String type_of_company;
	private String expertise_field;
	private String name_of_company;

	public int getid() {
		return id;
	}
	public void setId(int company_id) {
		this.id = company_id;
	}
	public String getType_of_company() {
		return type_of_company;
	}
	public void setType_of_company(String type_of_company) {
		this.type_of_company = type_of_company;
	}
	public String getExpertise_field() {
		return expertise_field;
	}
	public void setExpertise_field(String expertise_field) {
		this.expertise_field = expertise_field;
	}
	public String getName_of_company() {
		return name_of_company;
	}
	public void setName_of_company(String name_of_company) {
		this.name_of_company = name_of_company;
	}

}
