package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Trader extends User implements Serializable{
	@OneToMany(mappedBy="trader")
	private List<UpdatingTraderInformation>informations;
	
	private String Status;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<UpdatingTraderInformation> getInformations() {
		return informations;
	}

	public void setInformations(List<UpdatingTraderInformation> informations) {
		this.informations = informations;
	}
	

	
}
